package ayoko_lt.tag.home_page;

import android.os.Bundle;

import ayoko_lt.tag.AActivityWithWidgets;
import ayoko_lt.tag.R;

public class HomePage extends AActivityWithWidgets {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
    }

}
