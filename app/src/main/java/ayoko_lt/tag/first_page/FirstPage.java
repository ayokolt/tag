package ayoko_lt.tag.first_page;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import ayoko_lt.tag.R;
import ayoko_lt.tag.home_page.HomePage;

public class FirstPage extends AppCompatActivity {

    private TextView title;

    private static final int splash_time_out = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_page);

        title = (TextView) findViewById(R.id.title);
        Typeface titleFont = Typeface.createFromAsset(getAssets(), "fonts/bellerose.ttf");
        title.setTypeface(titleFont);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(FirstPage.this, HomePage.class);
                startActivity(i);
                finish();
            }
        } , splash_time_out);
    }
}
