package ayoko_lt.tag.search_results_page;

import android.os.Bundle;

import ayoko_lt.tag.AActivityWithWidgets;
import ayoko_lt.tag.R;

public class SearchResultsPage extends AActivityWithWidgets {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results_page);
    }

}
